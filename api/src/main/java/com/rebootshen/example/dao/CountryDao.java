package com.rebootshen.example.dao;

import java.util.List;

import com.rebootshen.example.model.Country;
import com.rebootshen.example.model.Language;

public interface CountryDao extends CoreDao<Country, Long> {


    List<Language> getLanguagesByCountryId(Long cid);
}