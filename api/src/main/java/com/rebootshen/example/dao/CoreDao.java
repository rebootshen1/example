package com.rebootshen.example.dao;


import java.io.Serializable;
import java.util.List;

import com.rebootshen.example.model.PkEntity;


public interface CoreDao<E extends PkEntity, K extends Serializable> {

    Class getEntityClass();

    void save(E e);

    void addAll(List<E> list);

    void deleteAll();

    void delete(E e);

    List<E> getAll();

    E getById(K id);

    E getByName(String name);
}
