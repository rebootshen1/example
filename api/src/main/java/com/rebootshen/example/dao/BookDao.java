package com.rebootshen.example.dao;

import java.util.List;

import com.rebootshen.example.model.Book;

public interface BookDao {

	/*
	 * CREATE and UPDATE
	 */
	public Book saveBook(Book book); // create and update

	/*
	 * READ
	 */
	public List<Book> listBooks();
	public Book getBook(Long id);

	/*
	 * DELETE
	 */
	public void deleteBook(Long id);
}
