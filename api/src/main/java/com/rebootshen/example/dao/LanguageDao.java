package com.rebootshen.example.dao;

import com.rebootshen.example.model.Language;


public interface LanguageDao extends CoreDao<Language, Long> {

}
