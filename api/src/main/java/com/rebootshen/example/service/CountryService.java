package com.rebootshen.example.service;

import java.util.List;

import com.rebootshen.example.model.Country;
import com.rebootshen.example.model.Language;


public interface CountryService extends CoreService<Country, Long> {

    public List<Language> getLanguages(Country country);

    public List<Language> getLanguagesByCountryId(Long cid);

    public List<Country> getCountriesByLanguageId(Long lid);
}
