package com.rebootshen.example.service;

import java.util.List;

import com.rebootshen.example.model.Book;


public interface BookService {

	/*
	 * CREATE and UPDATE 
	 */
	public Book saveBook(Book book);

	/*
	 * READ
	 */
	public List<Book> listBooks();
	public Book getBook(Long id);

	/*
	 * DELETE
	 */
	public void deleteBook(Long id);

}
