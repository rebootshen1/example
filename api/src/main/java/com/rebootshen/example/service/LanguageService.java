package com.rebootshen.example.service;


import java.util.List;

import com.rebootshen.example.model.Country;
import com.rebootshen.example.model.Language;


public interface LanguageService extends CoreService<Language, Long> {

    public List<Country> getCountries(Language language);
}
