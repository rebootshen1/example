package com.rebootshen.example.service;

import org.springframework.transaction.annotation.Transactional;

import com.rebootshen.example.dao.CoreDao;
import com.rebootshen.example.model.PkEntity;

import java.io.Serializable;
import java.util.List;

@Transactional
public abstract class AbstractBasicService<E extends PkEntity, K extends Serializable, D extends CoreDao<E, K>> implements CoreService<E, K> {

    public abstract D getDao();

    @Override
    public E getById(K id) {
        return getDao().getById(id);
    }

    @Override
    @Transactional(readOnly = true)
    public E getByName(String name) {
        return getDao().getByName(name);
    }

    @Override
    public E save(E entity) {
        getDao().save(entity);
        return entity;
    }

    @Override
    @Transactional
    public void delete(E entity) {
        getDao().delete(entity);
    }

    @Override
    @Transactional(readOnly = true)
    public List<E> getAll() {
        return getDao().getAll();
    }

    @Override
    @Transactional
    public void deleteAll() {
        getDao().deleteAll();
    }

    @Override
    @Transactional
    public void saveAll(List<E> entities) {
        getDao().addAll(entities);
    }
}
