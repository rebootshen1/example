package com.rebootshen.example.model;


import java.io.Serializable;


public interface PkEntity extends Serializable {
    public Long getId();
    public void setId(Long id);

    public String getName();

}
