package com.rebootshen.example.dao;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.List;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.Rollback;

import com.rebootshen.example.dao.BookDao;
import com.rebootshen.example.model.Book;

 
public class BookDaoTest extends BaseDaoTest {

	@Autowired
	private BookDao bookDao;
	 
	@Test
	@Rollback(value=true)
	public void testSaveBook()throws Exception{
	
		Book book = new Book();
		Book bookSaved ;
		
		book.setAuthors("Test1");
		book.setName("test book");
		book.setCode("112");
		
		assertNotNull(bookDao);
		bookSaved = bookDao.saveBook(book);
		 
		assertNotNull(bookSaved);	 
		assertNotNull(bookSaved.getId());	 
		assertEquals(bookSaved.getAuthors(), book.getAuthors());
		log.info("bookSaved.getName():{}",bookSaved.getName());
	
	}
 

	@Test
	@Rollback(value=true)
	public void testListBooks() throws Exception {
	 
		assertNotNull(bookDao);
		 
		List books = bookDao.listBooks();
		 
		assertNotNull(books);
		assertTrue(books.size() >= 0);
		log.info("books.size():{}",books.size());
	 
	}
 
}