package com.rebootshen.example.service;


import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.Rollback;

import com.rebootshen.example.model.Book;
import com.rebootshen.example.service.BookService;



public class BookServiceTest extends BaseServiceTest {

	@Autowired
	private BookService bookService;
	
	
	@Test
	@Rollback(value=true)
	public void testSaveBookAndGetBook()throws Exception{

		Book book = new Book();
		Book bookSaved ;
		
		Book bookGot ;
		
		book.setAuthors("Test1");
		book.setName("test book");
		book.setCode("112");
		
		assertNotNull(bookService);
		bookSaved = bookService.saveBook(book);
		 
		assertNotNull(bookSaved);	 
		assertNotNull(bookSaved.getId());	
		assertEquals(bookSaved.getAuthors(), book.getAuthors());
		
		log.info("bookSaved.getId() :{} ",bookSaved.getId());
		
		bookGot = bookService.getBook(bookSaved.getId());
		
		assertNotNull(bookGot);
		
		log.info("bookGot.getName() :{} ",bookGot.getName());


	}

}
