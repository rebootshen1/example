function addCountry() {
	$('#countryDialog').dialog("option", "title", 'Add Country');
	$('#countryDialog').dialog('open');
}

function editCountry(id) {

	$.get("get/" + id, function(result) {

		$("#countryDialog").html(result);

		$('#countryDialog').dialog("option", "title", 'Edit Country');

		$("#countryDialog").dialog('open');

		initializeDatePicker();
	});
}

function initializeDatePicker() {
	$(".datepicker").datepicker({
		dateFormat : "yy-mm-dd",
		changeMonth : true,
		changeYear : true,
		showButtonPanel : true
	});
}

function resetDialog(form) {

	form.find("input").val("");
}

$(document).ready(function() {

	$('#countryDialog').dialog({

		autoOpen : false,
		position : 'center',
		modal : true,
		resizable : false,
		width : 440,
		buttons : {
			"Save" : function() {
				$('#countryForm').submit();
			},
			"Cancel" : function() {
				$(this).dialog('close');
			}
		},
		close : function() {

			resetDialog($('#countryForm'));

			$(this).dialog('close');
		}
	});

	initializeDatePicker();
});
