package com.rebootshen.example.controller;

import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.rebootshen.example.model.Book;
import com.rebootshen.example.service.BookService;

@Controller
@RequestMapping(value = { "","/", "/book" })
//@Api(value = "/book", description = "book query") // Swagger
public class BookController {
	private static Logger log = LogManager.getLogger(BookController.class);
	private static Gson gson = new GsonBuilder().setPrettyPrinting().create();
	
	@Autowired
	private BookService bookService;

   // @ApiOperation(value = "get a list of books", response = Book.class, notes = "Returns all books ")
	@RequestMapping(value = { "","/", "/listBooks" },method = RequestMethod.GET)
	public String listBooks(Map<String, Object> map) {

		map.put("book", new Book());
		map.put("bookList", bookService.listBooks());

		return "book/listBooks";
	}

	@RequestMapping(value = "/get/{bookId}",
			method = RequestMethod.GET,  
			produces = { MediaType.TEXT_PLAIN_VALUE,MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE })
	public String getBook(@PathVariable Long bookId, Map<String, Object> map) {

		Book book = bookService.getBook(bookId);

		map.put("book", book);

		return "book/bookForm";
	}
	
	@RequestMapping(value = "/view/{bookId}",
			method = RequestMethod.GET,  
			produces = { MediaType.APPLICATION_JSON_VALUE})
	@ResponseBody
	public Book viewBook(@PathVariable Long bookId) {

		Book book = bookService.getBook(bookId);

		return book;
	}
	
	@RequestMapping(value = "/save", method = RequestMethod.POST)
    //@ApiOperation(value = "add book", httpMethod = "POST",  notes = "add book")
	//public String saveBook(@ApiParam(required = true,name = "bookData", value = "new book data" )@ModelAttribute("book") Book book,
	//		BindingResult result) {

	
	public String saveBook(@ModelAttribute("book") Book book,
			BindingResult result) {

		bookService.saveBook(book);

		/*
		 * Note that there is no slash "/" right after "redirect:" So, it
		 * redirects to the path relative to the current path
		 */
		return "redirect:listBooks";
	}
	
	@RequestMapping(value = "/update", 
			method = RequestMethod.POST,
			produces = { MediaType.APPLICATION_JSON_VALUE})
	@ResponseBody
	public Book updateBook(@RequestBody Book book,
			BindingResult result) {

		log.info(gson.toJson(book));
		Book newBook = bookService.saveBook(book);

		return newBook;
	}

	@RequestMapping(value = "/delete/{bookId}", method = RequestMethod.GET)
	public String deleteBook(@PathVariable("bookId") Long id) {

		bookService.deleteBook(id);

		/*
		 * redirects to the path relative to the current path
		 */
		// return "redirect:../listBooks";

		/*
		 * Note that there is the slash "/" right after "redirect:" So, it
		 * redirects to the path relative to the project root path
		 */
		return "redirect:/listBooks";
	}
}
