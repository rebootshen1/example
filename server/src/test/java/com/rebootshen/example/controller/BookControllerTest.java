package com.rebootshen.example.controller;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;

import org.hamcrest.CoreMatchers;
import org.junit.Test;
import org.mockito.Matchers;
import org.springframework.test.web.servlet.MvcResult;


public class BookControllerTest extends BaseControllerTest{
    
    @Test
    public void testJs() throws Exception {
    	
    	mockMvc.perform(get("/web-resources/js/js-for-listBooks.js"))  
        .andExpect(status().isOk()) 
        .andExpect(content().string(CoreMatchers.containsString("book")));
    }
    
    //@Test
    public void testList() throws Exception {
    	
    	MvcResult result = mockMvc.perform(get("/book/get/2"))
		 .andExpect(view().name("book/bookForm"))
		 .andExpect(forwardedUrl("/WEB-INF/views/book/bookForm.jsp"))
		 .andExpect(model().attributeExists("book"))
		 .andExpect(content().string(Matchers.contains("vegan")))
    	 .andExpect(status().isOk())
    	 .andDo(print())
    	 .andReturn();
    	
    	String content = result.getResponse().getContentAsString();
    	log.info("content:{}",content);
    }
}
