package com.rebootshen.example.controller;


import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.ContextHierarchy;
import org.springframework.test.context.junit4.AbstractJUnit4SpringContextTests;
import org.springframework.test.context.junit4.AbstractTransactionalJUnit4SpringContextTests;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.context.WebApplicationContext;

@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration(value = "src/main/webapp")  
@ContextConfiguration(locations = {
		"classpath:test-context.xml",
		"classpath:servlet-context.xml"
})
//@ContextHierarchy({  
//    @ContextConfiguration(name = "parent", locations = "classpath:spring-config.xml"),  
//    @ContextConfiguration(name = "child", locations = "classpath:spring-mvc.xml")  
//}) 
public class BaseControllerTest {
	Logger log = LogManager.getLogger(BaseControllerTest.class);
	
    @Autowired  
    private WebApplicationContext wac;  
    protected MockMvc mockMvc; 
    
    @Before  
    public void setUp() {  
        mockMvc = MockMvcBuilders.webAppContextSetup(wac).build();  
    }  
    
	@Test
	public void index() {
		
	}
}
