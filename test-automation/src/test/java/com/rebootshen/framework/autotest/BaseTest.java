package com.rebootshen.framework.autotest;

import java.io.IOException;
import java.net.URL;
import java.util.Set;

import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;

public class BaseTest {
	WebDriver driver;
	String baseUrl;
	StringBuffer verificationErrors = new StringBuffer();

    @BeforeClass
    public static void setupClass() {
        //MarionetteDriverManager.getInstance().setup();
        //System.setProperty("webdriver.gecko.driver","/var/lib/jenkins/driver/geckodriver-0.8.0-linux64");
    }
    
	@Before
	public void setUp() throws Exception {	
		DesiredCapabilities capability = DesiredCapabilities.firefox();
		driver = new RemoteWebDriver(new URL("http://selenium-hub:4444/wd/hub"), capability);	
	}
	
	@After
	public void tearDown() throws Exception {
		if(null != driver) {
			driver.quit();
		}

		String verificationErrorString = verificationErrors.toString();
		if (!"".equals(verificationErrorString)) {
			System.out.println(verificationErrorString);
		}
	}
	
	protected boolean switchWindow(String title) throws IOException {

		String currentWindow = driver.getWindowHandle();
		Set<String> availableWindows = driver.getWindowHandles();
		if (!availableWindows.isEmpty()) {
			for (String windowId : availableWindows) {
				 System.out.println("windowId:"+windowId+" title:{}"+driver.switchTo().window(windowId).getTitle());
				 
				if (driver.switchTo().window(windowId).getTitle().equals(title)) {
					return true;
				} else {
					driver.switchTo().window(currentWindow);
				}
			}
		}

		return false;
	}
}
