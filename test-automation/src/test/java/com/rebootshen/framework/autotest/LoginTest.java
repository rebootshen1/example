package com.rebootshen.framework.autotest;

import static org.junit.Assert.*;

import java.io.File;

import org.apache.commons.io.FileUtils;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;

public class LoginTest extends BaseTest {
	
	@Test
	public void testBookList() throws Exception {
		baseUrl = "http://192.168.99.100:8080/example/";
		driver.get(baseUrl);
		String buttonName = driver.findElement(By.xpath("/html/body/div[1]/button")).getText();
		
		Thread.sleep(5000);
		
		File scrFile = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
		// Now you can do whatever you need to do with it, for example copy somewhere
		//String tmpDir = System.getProperty("java.io.tmpdir");
		String tmpDir = "/tmp";
		System.out.println("tmpDir:"+tmpDir);
		System.out.println("buttonName:"+buttonName);
		FileUtils.copyFile(scrFile, new File(tmpDir ,"login-success.png"));

		String url = driver.getCurrentUrl();
		System.out.println("Title:"+ driver.getTitle());
		System.out.println("CurrentUrl:"+ url);
		
		assertTrue(buttonName.endsWith("Add Book"));

	}

	//@Test
	public void testLoginFailure() throws Exception {
		baseUrl = "http://192.168.99.100:8080/fx/panel/index.html";
		driver.get(baseUrl);
		
		driver.findElement(By.className("uname")).clear();
		driver.findElement(By.className("uname")).sendKeys("test1");
		driver.findElement(By.className("pword")).clear();
		driver.findElement(By.className("pword")).sendKeys("member");
		driver.findElement(By.xpath("/html/body/div/div/section/div/div[1]/div[2]/form/button")).click();
		
		
		Thread.sleep(2000);
		
		File scrFile = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
		// Now you can do whatever you need to do with it, for example copy somewhere
		//String tmpDir = System.getProperty("java.io.tmpdir");
		String tmpDir = "/tmp";
		System.out.println("tmpDir:"+tmpDir);
		FileUtils.copyFile(scrFile, new File(tmpDir ,"login-failed.png"));
		
		System.out.println("alert:"+ driver.findElement(By.className("alert")).getText());
		
		assertTrue(driver.findElement(By.className("alert"))!= null);

	}
	
	//@Test
	public void testLoginSuccess() throws Exception {
		baseUrl = "http://192.168.99.100:8080/fx/panel/index.html";
		driver.get(baseUrl);
		
		//driver.findElement(By.className("uname")).clear();
		//driver.findElement(By.className("uname")).sendKeys("test");
		//driver.findElement(By.className("pword")).clear();
		//driver.findElement(By.className("pword")).sendKeys("member");
		driver.findElement(By.xpath("/html/body/div/div/section/div/div[1]/div[2]/form/button")).click();
		
		Thread.sleep(5000);
		
		File scrFile = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
		// Now you can do whatever you need to do with it, for example copy somewhere
		//String tmpDir = System.getProperty("java.io.tmpdir");
		String tmpDir = "/tmp";
		System.out.println("tmpDir:"+tmpDir);
		FileUtils.copyFile(scrFile, new File(tmpDir ,"login-success.png"));

		String url = driver.getCurrentUrl();
		System.out.println("Title:"+ driver.getTitle());
		System.out.println("CurrentUrl:"+ url);
		
		assertTrue(url.endsWith("home"));

	}
}
